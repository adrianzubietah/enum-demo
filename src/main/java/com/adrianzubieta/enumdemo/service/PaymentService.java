package com.adrianzubieta.enumdemo.service;

import com.adrianzubieta.enumdemo.domain.Payment;

import java.math.BigDecimal;

public interface PaymentService {

  BigDecimal getFinalAmount(Payment payment);

}
