package com.adrianzubieta.enumdemo.service.impl;

import com.adrianzubieta.enumdemo.domain.Payment;
import com.adrianzubieta.enumdemo.service.PaymentService;
import com.adrianzubieta.enumdemo.utils.RateUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
@RequiredArgsConstructor
public class PaymentServiceImpl implements PaymentService {

  @Override
  public BigDecimal getFinalAmount(Payment payment) {
    return RateUtil.amountLessRate(new BigDecimal("100"), new BigDecimal("10"));
  }

}
