package com.adrianzubieta.enumdemo.domain;

public enum PaymentType {
  CREDIT,
  DEBIT,
  CASH
}
