package com.adrianzubieta.enumdemo.service;

import com.adrianzubieta.enumdemo.EnumDemoApplicationTests;
import com.adrianzubieta.enumdemo.domain.Payment;
import com.adrianzubieta.enumdemo.domain.PaymentType;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import java.math.BigDecimal;
import java.math.RoundingMode;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class PaymentServiceTest extends EnumDemoApplicationTests {

  @Autowired
  private PaymentService paymentService;

  @Test
  public void getFinalAmount_withTypeCash_returnAmountWith5percentMore() {
    Payment payment = new Payment();
    payment.setAmount(new BigDecimal("100.00"));
    payment.setPaymentType(PaymentType.CASH);

    BigDecimal finalAmount = paymentService.getFinalAmount(payment);

    assertEquals(new BigDecimal("105.00"), finalAmount.setScale(2, RoundingMode.HALF_UP));
  }

  @Test
  public void getFinalAmount_withTypeDebit_returnAmountWith10percentMore() {
    Payment payment = new Payment();
    payment.setAmount(new BigDecimal("100.00"));
    payment.setPaymentType(PaymentType.DEBIT);

    BigDecimal finalAmount = paymentService.getFinalAmount(payment);

    assertEquals(new BigDecimal("110.00"), finalAmount.setScale(2, RoundingMode.HALF_UP));
  }

  @Test
  public void getFinalAmount_withTypeCredit_returnAmountWith10percentMore() {
    Payment payment = new Payment();
    payment.setAmount(new BigDecimal("100.00"));
    payment.setPaymentType(PaymentType.CREDIT);

    BigDecimal finalAmount = paymentService.getFinalAmount(payment);

    assertEquals(new BigDecimal("120.00"), finalAmount.setScale(2, RoundingMode.HALF_UP));
  }
}
